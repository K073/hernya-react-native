import React from 'react';
import {Image, ScrollView, View} from 'react-native';
import {connect} from "react-redux";
import {fetchList} from "./store/actions";

const mapStateToProps = state => {
  return {
    dataList: state.dataList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchList: () => dispatch(fetchList())
  }
};

class App extends React.Component {
  componentDidMount () {
    this.props.fetchList();
  }

  render() {
    alert(this.props.dataList);
    return (
      <ScrollView>
        {this.props.dataList.map( value => <View><Image source={{uri: value.url}}/>value.title</View>)}
      </ScrollView>
    );
  }
}

// в слепую сделад я фиг знает даже не запускал

export default connect(mapStateToProps, mapDispatchToProps)(App);