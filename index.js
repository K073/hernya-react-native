import { AppRegistry } from 'react-native';
import App from './App';
import React from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import reducer from './store/reducer';
const store = createStore(reducer);

const Application = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent('homework71', () => Application);
