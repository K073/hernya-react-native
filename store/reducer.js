import {FETCH_LIST_SUCCESS} from "./actions";

const initialState = {
  dataList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LIST_SUCCESS:
      return state.dataList = action.resp;
    default:
      return state
  }
};

export default reducer;