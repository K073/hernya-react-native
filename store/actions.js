import axios from '../axios-app';

export const FETCH_LIST_REQUEST = 'FETCH_LIST_REQUEST';
export const FETCH_LIST_SUCCESS = 'FETCH_LIST_SUCCESS';
export const FETCH_LIST_ERROR = 'FETCH_LIST_ERROR';

export const fetchListRequest = () => {
  return { type: FETCH_LIST_REQUEST };
};

export const fetchListSuccess = (resp) => {
  return { type: FETCH_LIST_SUCCESS, resp};
};

export const fetchListError = () => {
  return { type: FETCH_LIST_ERROR };
};

export const fetchList = () => {
  return dispatch => {
    dispatch(fetchListRequest());
    axios.get().then(response => {
      dispatch(fetchListSuccess(response.data));
    }, error => {
      dispatch(fetchListError());
    });
  }
};